package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.UsersMapper;
import com.ruoyi.system.domain.Users;
import com.ruoyi.system.service.IUsersService;
import com.ruoyi.common.core.text.Convert;

/**
 * 用户表单Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
@Service
public class UsersServiceImpl implements IUsersService 
{
    @Autowired
    private UsersMapper usersMapper;

    /**
     * 查询用户表单
     * 
     * @param userId 用户表单ID
     * @return 用户表单
     */
    @Override
    public Users selectUsersById(Long userId)
    {
        return usersMapper.selectUsersById(userId);
    }

    /**
     * 查询用户表单列表
     * 
     * @param users 用户表单
     * @return 用户表单
     */
    @Override
    public List<Users> selectUsersList(Users users)
    {
        return usersMapper.selectUsersList(users);
    }

    /**
     * 新增用户表单
     * 
     * @param users 用户表单
     * @return 结果
     */
    @Override
    public int insertUsers(Users users)
    {
        return usersMapper.insertUsers(users);
    }

    /**
     * 修改用户表单
     * 
     * @param users 用户表单
     * @return 结果
     */
    @Override
    public int updateUsers(Users users)
    {
        return usersMapper.updateUsers(users);
    }

    /**
     * 删除用户表单对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteUsersByIds(String ids)
    {
        return usersMapper.deleteUsersByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除用户表单信息
     * 
     * @param userId 用户表单ID
     * @return 结果
     */
    @Override
    public int deleteUsersById(Long userId)
    {
        return usersMapper.deleteUsersById(userId);
    }
}
