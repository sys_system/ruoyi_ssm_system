package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Users;

/**
 * 用户表单Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
public interface UsersMapper 
{
    /**
     * 查询用户表单
     * 
     * @param userId 用户表单ID
     * @return 用户表单
     */
    public Users selectUsersById(Long userId);

    /**
     * 查询用户表单列表
     * 
     * @param users 用户表单
     * @return 用户表单集合
     */
    public List<Users> selectUsersList(Users users);

    /**
     * 新增用户表单
     * 
     * @param users 用户表单
     * @return 结果
     */
    public int insertUsers(Users users);

    /**
     * 修改用户表单
     * 
     * @param users 用户表单
     * @return 结果
     */
    public int updateUsers(Users users);

    /**
     * 删除用户表单
     * 
     * @param userId 用户表单ID
     * @return 结果
     */
    public int deleteUsersById(Long userId);

    /**
     * 批量删除用户表单
     * 
     * @param userIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteUsersByIds(String[] userIds);
}
