package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Based;

/**
 * 数据字典Mapper接口
 * 
 * @author ruoyi
 * @date 2021-05-27
 */
public interface BasedMapper 
{
    /**
     * 查询数据字典
     * 
     * @param dictId 数据字典ID
     * @return 数据字典
     */
    public Based selectBasedById(String dictId);

    /**
     * 查询数据字典列表
     * 
     * @param based 数据字典
     * @return 数据字典集合
     */
    public List<Based> selectBasedList(Based based);

    /**
     * 新增数据字典
     * 
     * @param based 数据字典
     * @return 结果
     */
    public int insertBased(Based based);

    /**
     * 修改数据字典
     * 
     * @param based 数据字典
     * @return 结果
     */
    public int updateBased(Based based);

    /**
     * 删除数据字典
     * 
     * @param dictId 数据字典ID
     * @return 结果
     */
    public int deleteBasedById(String dictId);

    /**
     * 批量删除数据字典
     * 
     * @param dictIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteBasedByIds(String[] dictIds);
}
