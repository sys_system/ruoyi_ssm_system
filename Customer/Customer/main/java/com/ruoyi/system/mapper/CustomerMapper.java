package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.Customer;

/**
 * 客户表单Mapper接口
 */
public interface CustomerMapper 
{
    /**
     * 查询客户表单
     * 
     * @param custId 客户表单ID
     * @return 客户表单
     */
    public Customer selectCustomerById(Long custId);

    /**
     * 查询客户表单列表
     * 
     * @param customer 客户表单
     * @return 客户表单集合
     */
    public List<Customer> selectCustomerList(Customer customer);

    /**
     * 新增客户表单
     * 
     * @param customer 客户表单
     * @return 结果
     */
    public int insertCustomer(Customer customer);

    /**
     * 修改客户表单
     * 
     * @param customer 客户表单
     * @return 结果
     */
    public int updateCustomer(Customer customer);

    /**
     * 删除客户表单
     * 
     * @param custId 客户表单ID
     * @return 结果
     */
    public int deleteCustomerById(Long custId);

    /**
     * 批量删除客户表单
     * 
     * @param custIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteCustomerByIds(String[] custIds);
}
