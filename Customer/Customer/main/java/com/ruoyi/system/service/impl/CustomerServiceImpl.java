package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.CustomerMapper;
import com.ruoyi.system.domain.Customer;
import com.ruoyi.system.service.ICustomerService;
import com.ruoyi.common.core.text.Convert;

/**
 * 客户表单Service业务层处理
 */
@Service
public class CustomerServiceImpl implements ICustomerService 
{
    @Autowired
    private CustomerMapper customerMapper;

    /**
     * 查询客户表单
     * 
     * @param custId 客户表单ID
     * @return 客户表单
     */
    @Override
    public Customer selectCustomerById(Long custId)
    {
        return customerMapper.selectCustomerById(custId);
    }

    /**
     * 查询客户表单列表
     * 
     * @param customer 客户表单
     * @return 客户表单
     */
    @Override
    public List<Customer> selectCustomerList(Customer customer)
    {
        return customerMapper.selectCustomerList(customer);
    }

    /**
     * 新增客户表单
     * 
     * @param customer 客户表单
     * @return 结果
     */
    @Override
    public int insertCustomer(Customer customer)
    {
        return customerMapper.insertCustomer(customer);
    }

    /**
     * 修改客户表单
     * 
     * @param customer 客户表单
     * @return 结果
     */
    @Override
    public int updateCustomer(Customer customer)
    {
        return customerMapper.updateCustomer(customer);
    }

    /**
     * 删除客户表单对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCustomerByIds(String ids)
    {
        return customerMapper.deleteCustomerByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除客户表单信息
     * 
     * @param custId 客户表单ID
     * @return 结果
     */
    @Override
    public int deleteCustomerById(Long custId)
    {
        return customerMapper.deleteCustomerById(custId);
    }
}
