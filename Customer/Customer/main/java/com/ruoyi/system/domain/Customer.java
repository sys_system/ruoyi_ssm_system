package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 客户表单对象 customer
 */
public class Customer extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 客户编号(主键) */
    private Long custId;

    /** 客户名称 */
    @Excel(name = "客户名称")
    private String custName;

    /** 负责人id */
    @Excel(name = "负责人id")
    private Long custUserId;

    /** 创建人id */
    @Excel(name = "创建人id")
    private Long custCreateId;

    /** 客户信息来源 */
    @Excel(name = "客户信息来源")
    private String custSource;

    /** 客户所属行业 */
    @Excel(name = "客户所属行业")
    private String custIndustry;

    /** 客户级别 */
    @Excel(name = "客户级别")
    private String custLevel;

    /** 联系人 */
    @Excel(name = "联系人")
    private String custLinkman;

    /** 固定电话 */
    @Excel(name = "固定电话")
    private String custPhone;

    /** 移动电话 */
    @Excel(name = "移动电话")
    private String custMobile;

    /** 邮政编码 */
    @Excel(name = "邮政编码")
    private String custZipcode;

    /** 联系地址 */
    @Excel(name = "联系地址")
    private String custAddress;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "创建时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date custCreatetime;

    public void setCustId(Long custId) 
    {
        this.custId = custId;
    }

    public Long getCustId() 
    {
        return custId;
    }
    public void setCustName(String custName) 
    {
        this.custName = custName;
    }

    public String getCustName() 
    {
        return custName;
    }
    public void setCustUserId(Long custUserId) 
    {
        this.custUserId = custUserId;
    }

    public Long getCustUserId() 
    {
        return custUserId;
    }
    public void setCustCreateId(Long custCreateId) 
    {
        this.custCreateId = custCreateId;
    }

    public Long getCustCreateId() 
    {
        return custCreateId;
    }
    public void setCustSource(String custSource) 
    {
        this.custSource = custSource;
    }

    public String getCustSource() 
    {
        return custSource;
    }
    public void setCustIndustry(String custIndustry) 
    {
        this.custIndustry = custIndustry;
    }

    public String getCustIndustry() 
    {
        return custIndustry;
    }
    public void setCustLevel(String custLevel) 
    {
        this.custLevel = custLevel;
    }

    public String getCustLevel() 
    {
        return custLevel;
    }
    public void setCustLinkman(String custLinkman) 
    {
        this.custLinkman = custLinkman;
    }

    public String getCustLinkman() 
    {
        return custLinkman;
    }
    public void setCustPhone(String custPhone) 
    {
        this.custPhone = custPhone;
    }

    public String getCustPhone() 
    {
        return custPhone;
    }
    public void setCustMobile(String custMobile) 
    {
        this.custMobile = custMobile;
    }

    public String getCustMobile() 
    {
        return custMobile;
    }
    public void setCustZipcode(String custZipcode) 
    {
        this.custZipcode = custZipcode;
    }

    public String getCustZipcode() 
    {
        return custZipcode;
    }
    public void setCustAddress(String custAddress) 
    {
        this.custAddress = custAddress;
    }

    public String getCustAddress() 
    {
        return custAddress;
    }
    public void setCustCreatetime(Date custCreatetime) 
    {
        this.custCreatetime = custCreatetime;
    }

    public Date getCustCreatetime() 
    {
        return custCreatetime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("custId", getCustId())
            .append("custName", getCustName())
            .append("custUserId", getCustUserId())
            .append("custCreateId", getCustCreateId())
            .append("custSource", getCustSource())
            .append("custIndustry", getCustIndustry())
            .append("custLevel", getCustLevel())
            .append("custLinkman", getCustLinkman())
            .append("custPhone", getCustPhone())
            .append("custMobile", getCustMobile())
            .append("custZipcode", getCustZipcode())
            .append("custAddress", getCustAddress())
            .append("custCreatetime", getCustCreatetime())
            .toString();
    }
}
