-- 菜单 SQL

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('数据字典', '2000', '1', '/system/based', 'C', '0', 'system:based:view', '#', 'admin', sysdate(), '', null, '数据字典菜单');

-- 按钮父菜单ID
SELECT @parentId := LAST_INSERT_ID();

-- 按钮 SQL
insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('数据字典查询', @parentId, '1',  '#',  'F', '0', 'system:based:list',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('数据字典新增', @parentId, '2',  '#',  'F', '0', 'system:based:add',          '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('数据字典修改', @parentId, '3',  '#',  'F', '0', 'system:based:edit',         '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('数据字典删除', @parentId, '4',  '#',  'F', '0', 'system:based:remove',       '#', 'admin', sysdate(), '', null, '');

insert into sys_menu (menu_name, parent_id, order_num, url, menu_type, visible, perms, icon, create_by, create_time, update_by, update_time, remark)
values('数据字典导出', @parentId, '5',  '#',  'F', '0', 'system:based:export',       '#', 'admin', sysdate(), '', null, '');
