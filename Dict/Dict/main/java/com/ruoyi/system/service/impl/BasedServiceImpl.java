package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.BasedMapper;
import com.ruoyi.system.domain.Based;
import com.ruoyi.system.service.IBasedService;
import com.ruoyi.common.core.text.Convert;

/**
 * 数据字典Service业务层处理
 */
@Service
public class BasedServiceImpl implements IBasedService 
{
    @Autowired
    private BasedMapper basedMapper;

    /**
     * 查询数据字典
     * 
     * @param dictId 数据字典ID
     * @return 数据字典
     */
    @Override
    public Based selectBasedById(String dictId)
    {
        return basedMapper.selectBasedById(dictId);
    }

    /**
     * 查询数据字典列表
     * 
     * @param based 数据字典
     * @return 数据字典
     */
    @Override
    public List<Based> selectBasedList(Based based)
    {
        return basedMapper.selectBasedList(based);
    }

    /**
     * 新增数据字典
     * 
     * @param based 数据字典
     * @return 结果
     */
    @Override
    public int insertBased(Based based)
    {
        return basedMapper.insertBased(based);
    }

    /**
     * 修改数据字典
     * 
     * @param based 数据字典
     * @return 结果
     */
    @Override
    public int updateBased(Based based)
    {
        return basedMapper.updateBased(based);
    }

    /**
     * 删除数据字典对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteBasedByIds(String ids)
    {
        return basedMapper.deleteBasedByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除数据字典信息
     * 
     * @param dictId 数据字典ID
     * @return 结果
     */
    @Override
    public int deleteBasedById(String dictId)
    {
        return basedMapper.deleteBasedById(dictId);
    }
}
