package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 数据字典对象 based
 */
public class Based extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 数据字典id(主键) */
    private String dictId;

    /** 数据字典类别代码 */
    @Excel(name = "数据字典类别代码")
    private String dictTypeCode;

    /** 数据字典类别名称 */
    @Excel(name = "数据字典类别名称")
    private String dictTypeName;

    /** 数据字典项目名称 */
    @Excel(name = "数据字典项目名称")
    private String dictItemName;

    /** 数据字典项目代码(可为空) */
    @Excel(name = "数据字典项目代码(可为空)")
    private String dictItemCode;

    /** 排序字段 */
    @Excel(name = "排序字段")
    private Integer dictSort;

    /** 1:使用 0:停用 */
    @Excel(name = "1:使用 0:停用")
    private String dictEnable;

    /** 备注 */
    @Excel(name = "备注")
    private String dictMemo;

    public void setDictId(String dictId) 
    {
        this.dictId = dictId;
    }

    public String getDictId() 
    {
        return dictId;
    }
    public void setDictTypeCode(String dictTypeCode) 
    {
        this.dictTypeCode = dictTypeCode;
    }

    public String getDictTypeCode() 
    {
        return dictTypeCode;
    }
    public void setDictTypeName(String dictTypeName) 
    {
        this.dictTypeName = dictTypeName;
    }

    public String getDictTypeName() 
    {
        return dictTypeName;
    }
    public void setDictItemName(String dictItemName) 
    {
        this.dictItemName = dictItemName;
    }

    public String getDictItemName() 
    {
        return dictItemName;
    }
    public void setDictItemCode(String dictItemCode) 
    {
        this.dictItemCode = dictItemCode;
    }

    public String getDictItemCode() 
    {
        return dictItemCode;
    }
    public void setDictSort(Integer dictSort) 
    {
        this.dictSort = dictSort;
    }

    public Integer getDictSort() 
    {
        return dictSort;
    }
    public void setDictEnable(String dictEnable) 
    {
        this.dictEnable = dictEnable;
    }

    public String getDictEnable() 
    {
        return dictEnable;
    }
    public void setDictMemo(String dictMemo) 
    {
        this.dictMemo = dictMemo;
    }

    public String getDictMemo() 
    {
        return dictMemo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dictId", getDictId())
            .append("dictTypeCode", getDictTypeCode())
            .append("dictTypeName", getDictTypeName())
            .append("dictItemName", getDictItemName())
            .append("dictItemCode", getDictItemCode())
            .append("dictSort", getDictSort())
            .append("dictEnable", getDictEnable())
            .append("dictMemo", getDictMemo())
            .toString();
    }
}
