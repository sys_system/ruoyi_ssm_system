package com.ruoyi.system.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Based;
import com.ruoyi.system.service.IBasedService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 数据字典Controller
 */
@Controller
@RequestMapping("/system/based")
public class BasedController extends BaseController
{
    private String prefix = "system/based";

    @Autowired
    private IBasedService basedService;

    @RequiresPermissions("system:based:view")
    @GetMapping()
    public String based()
    {
        return prefix + "/based";
    }

    /**
     * 查询数据字典列表
     */
    @RequiresPermissions("system:based:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Based based)
    {
        startPage();
        List<Based> list = basedService.selectBasedList(based);
        return getDataTable(list);
    }

    /**
     * 导出数据字典列表
     */
    @RequiresPermissions("system:based:export")
    @Log(title = "数据字典", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Based based)
    {
        List<Based> list = basedService.selectBasedList(based);
        ExcelUtil<Based> util = new ExcelUtil<Based>(Based.class);
        return util.exportExcel(list, "数据字典数据");
    }

    /**
     * 新增数据字典
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存数据字典
     */
    @RequiresPermissions("system:based:add")
    @Log(title = "数据字典", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Based based)
    {
        return toAjax(basedService.insertBased(based));
    }

    /**
     * 修改数据字典
     */
    @GetMapping("/edit/{dictId}")
    public String edit(@PathVariable("dictId") String dictId, ModelMap mmap)
    {
        Based based = basedService.selectBasedById(dictId);
        mmap.put("based", based);
        return prefix + "/edit";
    }

    /**
     * 修改保存数据字典
     */
    @RequiresPermissions("system:based:edit")
    @Log(title = "数据字典", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Based based)
    {
        return toAjax(basedService.updateBased(based));
    }

    /**
     * 删除数据字典
     */
    @RequiresPermissions("system:based:remove")
    @Log(title = "数据字典", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(basedService.deleteBasedByIds(ids));
    }
}
