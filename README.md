# ruoyi_ssm_system

#### 介绍
基于ruoyi的SSM客户管理系统

#### 系统要求
JDK >= 1.8
MySQL >= 5.7
Maven >= 3.0

#### 使用教程

1.  从gitee下载若依框架源码并导入
2.  结合SSM框架实现数据表的基本操作
3.  使用若依的生成器工具统一样式，集成到若依开发框架

#### 使用说明

1. 前往Gitee下载页面(https://gitee.com/y_project/RuoYi (opens new window))下载解压到工作目录，导入到Eclipse，菜单 File -> Import，然后选择 Maven -> Existing Maven Projects，点击 Next> 按钮，选择工作目录，然后点击 Finish 按钮，即可成功导入。Eclipse会自动加载Maven依赖包，初次加载会比较慢（根据自身网络情况而定）。创建数据库ry并导入数据脚本ry_2021xxxx.sql，quartz.sql，打开项目运行com.ruoyi.RuoYiApplication.java。
2. 修改数据库连接，编辑resources目录下的application-druid.yml
3. 修改服务器配置，编辑resources目录下的application.yml
4. 打开浏览器，输入：(http://localhost:80) （默认账户/密码 admin/admin123)


#### 特技

1. 系统环境
    Java EE 8
    Servlet 3.0
    Apache Maven 3
2. 主框架(rouyi)
    Spring Boot 2.2.x
    Spring Framework 5.2.x
    Apache Shiro 1.7
3. 持久层
    Apache MyBatis 3.5.x
    Hibernate Validation 6.0.x
    Alibaba Druid 1.2.x
4. 视图层
    Bootstrap 3.3.7
    Thymeleaf 3.0.x
